<?php

declare(strict_types=1);

namespace Paneric\Session\Infrastructure;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Persister;
use Paneric\Session\Model\SessionDAO;
use Paneric\Session\Model\SessionPersisterInterface;
use PDO;

class SessionPersister extends Persister implements SessionPersisterInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'session';
        $this->daoClass = SessionDAO::class;
        $this->fetchMode = PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE;
    }

    public function getManager(): Manager
    {
        return $this->manager;
    }

    public function removeOld(string $timeLimit): int
    {
        $stmt = $this->manager->setStmt(
            'DELETE FROM ' . $this->table . ' WHERE ses_updated_at < :old ',
            ['old' => $timeLimit]
        );

        return $stmt->rowCount();
    }
}
