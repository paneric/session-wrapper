<?php

declare(strict_types=1);

namespace Paneric\Session\Infrastructure;

use DateInterval;
use DateTimeImmutable;
use Exception;
use JsonException;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Session\Model\SessionDAO;
use Paneric\Session\Model\SessionPersisterInterface;
use Paneric\Session\Model\SessionRepositoryInterface;
use SessionHandlerInterface;

class SessionHandler implements SessionHandlerInterface
{
    protected array $config;
    protected SessionDAO $sessionDAO;
    protected array $data;
    protected array $flashData;
    protected string $sessionId;
    protected ?string $jwtSessionId = null;

    public function __construct(
        ConfigInterface $config,
        protected SessionRepositoryInterface $repository,
        protected SessionPersisterInterface $persister,
        protected GuardInterface $guard
    ) {
        $this->config = $config();
        $this->flashData = ['error' => [], 'warning' => [], 'info' => [], 'success' => [], 'value' => []];
    }

    public function sessionStart(string $jwtSessionId = null): void
    {
        $this->jwtSessionId = $jwtSessionId;

        session_set_save_handler($this, true);

        register_shutdown_function('session_write_close');

        ini_set('session.sid_bits_per_character', (string) $this->config['sid_bits_per_character']);
        ini_set('session.use_only_cookies', (string) $this->config['use_only_cookies']);
        ini_set('session.gc_maxlifetime', (string) $this->config['gc_maxlifetime']);

        session_set_cookie_params(
            $this->config['cookie_lifetime'],
            $this->config['cookie_access'],
            $this->config['domain'],
            $this->config['secure'],
            $this->config['js_denied']
        );

        session_name($this->config['cookie_name']);

        session_start();

        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_regenerate_id($this->config['regenerate']);
        }
    }

    public function open(string $path, string $name): bool
    {
        if ($this->persister->getManager()->getPdo()) {
            return true;
        }

        return false;
    }

    public function close(): bool
    {
        if ($this->persister->getManager()->getPdo()) {
            return false;
        }

        return true;
    }

    /**
     * @throws JsonException
     */
    public function read(string $id): string
    {
        $this->sessionId = $this->jwtSessionId ?? $id;

        $this->repository->adaptManager();

        /** @var SessionDAO $sessionDAO */
        $sessionDAO = $this->repository->findOneBySessionId($id);

        if ($sessionDAO === null) {
            $sessionDAO = new SessionDAO();
            $sessionDAO->setSessionId($this->sessionId);
            $sessionDAO->setIpAddress($_SERVER['REMOTE_ADDR']);
            $sessionDAO->setUserAgent($_SERVER['HTTP_USER_AGENT']);

            $this->sessionDAO = $sessionDAO;
            $this->data = [];

            return '';
        }

        $this->sessionDAO = $sessionDAO;
        $this->data = json_decode(
            $this->guard->decrypt($this->sessionDAO->getData()),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return '';
    }

    /**
     * @throws JsonException
     */
    public function write(string $id, string $data): bool
    {
        $this->sessionId = $this->jwtSessionId ?? $id;

        $this->sessionDAO->setData(
            $this->guard->encrypt(json_encode($this->data, JSON_THROW_ON_ERROR))
        );

        $sessionDAOid = $this->sessionDAO->getId();
        if($sessionDAOid === null) {
            $this->persister->create($this->sessionDAO);

            return true;
        }

        $attributes = $this->sessionDAO->convert();
        unset($attributes['id']);
        $session = new SessionDAO();
        $session->hydrate($attributes);
        $this->persister->update(['ses_id' => $sessionDAOid], $session);

        return true;
    }

    public function destroy(string $id): bool
    {
        $this->sessionId = $this->jwtSessionId ?? $id;

        $this->persister->delete(['ses_session_id' => $this->sessionId]);

        return true;
    }

    public function gc(int $max_lifetime): int|false
    {
        try {
            $now = new DateTimeImmutable('now');

            $old = $now->sub(new DateInterval('PT' . $max_lifetime . 'S'));

            $oldFormatted = $old->format('Y-m-d H:i:s');

            return $this->persister->removeOld($oldFormatted);
        } catch (Exception $e) {
        }

        return false;
    }
}
