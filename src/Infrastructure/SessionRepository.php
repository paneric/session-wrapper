<?php

declare(strict_types=1);

namespace Paneric\Session\Infrastructure;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;
use Paneric\Interfaces\DataObject\DataObjectInterface;
use Paneric\Session\Model\SessionDAO;
use Paneric\Session\Model\SessionRepositoryInterface;
use PDO;

class SessionRepository extends Repository implements SessionRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'session';
        $this->daoClass = SessionDAO::class;
        $this->fetchMode = PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE;
    }

    public function findOneBySessionId(string $sessionId): null|DataObjectInterface|array
    {
        $this->adaptManager();

        $queryResult = $this->manager->findOneBy(['ses_session_id' => $sessionId]);

        if ($queryResult === false) {
            return null;
        }

        return $queryResult;
    }
}
