<?php

declare(strict_types=1);

namespace Paneric\Session\Infrastructure;

use Paneric\Interfaces\Session\SessionInterface;

class Session extends SessionHandler implements SessionInterface
{
    public function setData($input, string $key = null): void
    {
        if (is_array($input) && ($key === null)) {
            $this->data = $input;
        }

        if ($key !== null && $input) {
            $this->data[$key] = $input;
        }
    }

    public function getData(string $key = null): array|string|int|null
    {
        if ($key === null) {
            return $this->data;
        }

        return $this->data[$key] ?? null;
    }

    public function unsetData(string $key = null): void
    {
        if ($key === null) {
            $this->data = [];

            return;
        }

        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }
    }

    public function setFlash(array $messages, string $key): void
    {
        if (array_key_exists($key, $this->flashData)) {
            if ($key === 'value') {
                $this->flashData[$key] = array_merge($this->flashData[$key], $messages);
                return;
            }

            foreach ($messages as $msg) {
                $this->flashData[$key][] = $msg;
            }
        }
    }

    public function getFlash(string $key = null): ?array
    {
        if ($key === null) {
            return $this->flashData;
        }

        return $this->flashData[$key] ?? null;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setJwtSessionId(string $jwtSessionId): void
    {
        $this->jwtSessionId = $jwtSessionId;
    }

    public function getJwtSessionId(): ?string
    {
        return $this->jwtSessionId;
    }
}
