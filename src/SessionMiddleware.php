<?php

declare(strict_types=1);

namespace Paneric\Session;

use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SessionMiddleware implements MiddlewareInterface
{
    public function __construct(protected ContainerInterface $container)
    {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var SessionInterface $session */
        $session = $this->container->get(SessionInterface::class);
        $jwtSessionId = $request->getAttribute('jwt_session_id');

        if ($jwtSessionId !== null) {
            $session->setJwtSessionId($jwtSessionId);
        }

        $session->sessionStart();
        $session->setData($this->container->get('local'), 'local');

        return $handler->handle($request);
    }
}
