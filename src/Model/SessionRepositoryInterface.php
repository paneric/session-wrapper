<?php

declare(strict_types=1);

namespace Paneric\Session\Model;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface SessionRepositoryInterface
{
    public function findOneBySessionId(string $sessionId): null|DataObjectInterface|array;
}
