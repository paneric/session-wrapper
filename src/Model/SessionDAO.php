<?php

declare(strict_types=1);

namespace Paneric\Session\Model;

use Paneric\DataObject\DAO;

class SessionDAO extends DAO
{
    protected null|int|string $id = null;

    protected string $sessionId;
    protected string $ipAddress;
    protected string $userAgent;
    protected string $data;

    public function __construct()
    {
        $this->prefix = 'ses_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getSessionId(): string
    {
        return $this->sessionId;
    }
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }
    public function getData(): string
    {
        return $this->data;
    }

    protected function setId(null|int|string $id): self
    {
        if ($id !== null && !is_int($id)) {
            $this->id = (int) $id;
            return $this;
        }
        $this->id = $id;
        return $this;
    }
    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;
        return $this;
    }
    public function setIpAddress(string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }
    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;
        return $this;
    }
    public function setData(string $data): self
    {
        $this->data = $data;
        return $this;
    }
}
