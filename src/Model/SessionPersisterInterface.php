<?php

declare(strict_types=1);

namespace Paneric\Session\Model;

use Paneric\DBAL\Manager;

interface SessionPersisterInterface
{
    public function getManager(): Manager;

    public function removeOld(string $timeLimit): int;
}
